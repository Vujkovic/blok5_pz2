﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using PZ2.Model;

namespace PZ2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private NetworkModel NetworkModel;

        public double StartX;
        public double StartY;
        public double EndX;
        public double EndY;

        GMapOverlay NodesOverlay = new GMapOverlay("Nodes_ID");
        GMapOverlay LinesOverlay = new GMapOverlay("Lines_ID");
        GMapOverlay SwitchesOverlay = new GMapOverlay("Switches_ID");
        GMapOverlay SubstationsOverlay = new GMapOverlay("Substations_ID");
        public MainWindow()
        {
            InitializeComponent();
            NetworkModel = new NetworkModel();
            try
            {
                XmlSerializer serializer = new XmlSerializer(NetworkModel.GetType());
                using (FileStream reader = new FileStream("Geographic.xml", FileMode.Open))
                {
                    NetworkModel = (NetworkModel)serializer.Deserialize(reader);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("Error reading Geographic.xml");
                this.Close();
            }
        }

        private void Form_Load(object sender, EventArgs e)
        {
            double blX = 45.2325;
            double blY = 19.793909;
            double trX = 45.277031;
            double trY = 19.894459;
            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            //gmap.SetPositionByKeywords("Maputo, Mozambique");
            gmap.Position = new PointLatLng((blX + trX) / 2, (blY + trY) / 2);

            foreach (var item in NetworkModel.Switches)
            {
                double lat = double.MinValue;
                double lon = double.MinValue;
                ToLatLon(item.X, item.Y, 34, out lat, out lon);
                GMapMarker gm = new GMarkerGoogle(new PointLatLng(lat, lon), GMarkerGoogleType.green)
                {
                    ToolTipText = item.Name + " " + item.GetType().ToString().Substring(10),
                    ToolTipMode = MarkerTooltipMode.OnMouseOver
                };
                SwitchesOverlay.Markers.Add(gm);
            }

            foreach (var item in NetworkModel.Substations)
            {
                double lat = double.MinValue;
                double lon = double.MinValue;
                ToLatLon(item.X, item.Y, 34, out lat, out lon);
                GMapMarker gm = new GMarkerGoogle(new PointLatLng(lat, lon), GMarkerGoogleType.yellow)
                {
                    ToolTipText = item.Name + " " + item.GetType().ToString().Substring(10),
                    ToolTipMode = MarkerTooltipMode.OnMouseOver
                };
                SubstationsOverlay.Markers.Add(gm);
            }

            foreach (var item in NetworkModel.Lines)
            {
                List<PointLatLng> points = new List<PointLatLng>();
                foreach (var vert in item.Vertices)
                {
                    ToLatLon(vert.X, vert.Y, 34, out double lat, out double lng);

                    PointLatLng pointLatLong = new PointLatLng
                    {
                        Lat = lat,
                        Lng = lng
                    };
                    points.Add(pointLatLong);

                }
                GMapRoute lines = new GMapRoute(points, "Route:" + item.Id);
                lines.Stroke.Width = 1;
                LinesOverlay.Routes.Add(lines);
            }

            foreach (var item in NetworkModel.Nodes)
            {
                double lat = double.MinValue;
                double lon = double.MinValue;
                double latNew = double.MinValue;
                double lonNew = double.MinValue;
                ToLatLon(item.X, item.Y, 34, out lat, out lon);

                ModifyCoordinates(lat, lon, out latNew, out lonNew);
                GMapMarker gm = new GMarkerGoogle(new PointLatLng(lat, lon), GMarkerGoogleType.red)
                {
                    ToolTipText = item.Name + " " + item.GetType().ToString().Substring(10),
                    ToolTipMode = MarkerTooltipMode.OnMouseOver
                };
                NodesOverlay.Markers.Add(gm);
            }

            gmap.Overlays.Add(SwitchesOverlay);
            gmap.Overlays.Add(SubstationsOverlay);
            gmap.Overlays.Add(NodesOverlay);
            gmap.Overlays.Add(LinesOverlay);

        }

        private void ModifyCoordinates(double lat, double lon, out double latNew, out double lonNew)
        {

            latNew = Math.Round(lat, 2, MidpointRounding.ToEven);
            lonNew = Math.Round(lon, 2, MidpointRounding.ToEven);

        }
        public static void ToLatLon(double utmX, double utmY, int zoneUTM, out double latitude, out double longitude)
        {
            bool isNorthHemisphere = true;

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = zoneUTM;
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (c_sa * 0.9996);
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
            latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
        }

    }
}
