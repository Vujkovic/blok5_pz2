﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ2.Model
{
    public class Point
    {
        double x;
        double y;

        public Point()
        {
        }

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
    }
}
