﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ2.Model
{
    public class NetworkModel
    {
        public List<SubstationEntity> Substations;
        public List<NodeEntity> Nodes;
        public List<SwitchEntity> Switches;
        public List<LineEntity> Lines;
    }
}
