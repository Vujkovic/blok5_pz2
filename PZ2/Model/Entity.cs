﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ2.Model
{
    public class Entity
    {
        private string id;
        private string name;

        public Entity()
        {
        }

        public string Name { get => name; set => name = value; }
        public string Id { get => id; set => id = value; }
    }
}
